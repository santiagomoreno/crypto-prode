const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Result Oracle", function () {
  let owner, user, user_2, user_3, user_4;

  beforeEach(async function () {
    [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

    const ResultOracle = await ethers.getContractFactory("ResultOracle", owner);
    this.resultOracle = await ResultOracle.deploy([user_3.address, user.address, user_2.address]);
  });

  describe("constructor() Function", function () {
    it("should revert when try to set the owner as jusge in first position", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([owner.address, user.address, user_2.address])).to.be.revertedWith("the owner cant be judge");
    });

    it("should revert when try to set the owner as jusge in third position", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, owner.address])).to.be.revertedWith("the owner cant be judge");
    });

    it("should revert when try to set more than 3 address as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, user_3.address, owner.address])).to.be.revertedWith("you must set 3 judges");
    });

    it("should revert when try to set address zero as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, ethers.constants.AddressZero])).to.be.revertedWith("a judge cant be address zero");
    });

    it("should revert when try to set same address as judge", async function () {
      const ResultOracle1 = await ethers.getContractFactory("ResultOracle", owner);

      await expect(ResultOracle1.deploy([user.address, user_2.address, user_2.address])).to.be.revertedWith("you can't set the same address twice as a judge");
    });

    it("should getJudge() function to give the address with the same order as saved in the constructor", async function () {
      expect(user_3.address).to.eq(await this.resultOracle.getJudge(0));
      expect(user.address).to.eq(await this.resultOracle.getJudge(1));
      expect(user_2.address).to.eq(await this.resultOracle.getJudge(2));
    });
  });

  describe("setNewJudge() Function", function () {
    it("should can set a correct index with a non used judge", async function () {
      await this.resultOracle.setNewJudge(0, user_4.address);

      expect(user_4.address).to.eq(await this.resultOracle.getJudge(0));
    });

    it("set jugde with not owner address", async function () {
      await expect(this.resultOracle.connect(user).setNewJudge(0, user_2.address)).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when thy to set again an existent judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, user_2.address)).to.be.revertedWith(
        "you can't set the same address twice as a judge");
    });

    it("should revert when try to set the owner as judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, owner.address)).to.be.revertedWith(
        "the owner cant be judge");
    });

    it("should revert when try to set address(0) as judge", async function () {
      await expect(this.resultOracle.setNewJudge(0, ethers.constants.AddressZero)).to.be.revertedWith(
        "a judge cant be address zero");
    });

    it.skip("should revert when time to set new judges expired", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1653879899]);
      //await ethers.provider.send("evm_mine", []);
      await expect(this.resultOracle.setNewJudge(0, ethers.constants.AddressZero)).to.be.revertedWith(
        "expired to set a judge");
    });
  });

  describe("getFirstRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getFirstRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
      //await ethers.provider.send("evm_mine", []);

      var vote = ethers.BigNumber.from("11110011");

      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getFirstRoundResult());

    });
  });

  describe("getSecondRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getSecondRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1660532401]);
      //await ethers.provider.send("evm_mine", []);

      var vote = ethers.BigNumber.from("1001");

      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getSecondRoundResult());
    });
  });

  describe("getThirdRoundResult() Function", function () {
    it("should revert when there is no result", async function () {
      await expect(this.resultOracle.getThirdRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return the same result that everyone vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1667271601]);
      //await ethers.provider.send("evm_mine", []);

      var vote = ethers.BigNumber.from("100");

      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getThirdRoundResult());
    });
  });

  describe("addJudgeDebateToFirstRound() Function", function () {
    it("should revert when try to add debate with a incorrect vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1651352844]);
      //await ethers.provider.send("evm_mine", []);
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(ethers.BigNumber.from("11122211"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it.skip("should revert when try to add debate with a incorrect vote", async function () {
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(ethers.BigNumber.from("1111"))).to.be.revertedWith(
        "Your vote is being made ahead of time");
    });

    it("should revert when try to add debate with a non judge", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
      //await ethers.provider.send("evm_mine", []);

      await expect(this.resultOracle.connect(user_4).addJudgeDebateToFirstRound(111)).to.be.revertedWith(
        "is not a judge");
    });

    it("should set correct debate when you set with correct data", async function () {
      var vote = ethers.BigNumber.from("10011001");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getFirstRoundResult());
    });

    it("should set correct debate when you set with zero", async function () {
      var vote = ethers.BigNumber.from("0");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getFirstRoundResult());
    });
  });

  describe("addJudgeDebateToSecondRound() Function", function () {
    it("should revert when try to add debate with a incorrect vote", async function () {
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(ethers.BigNumber.from("2222"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it.skip("should revert when try to add debate with a incorrect vote", async function () {
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(ethers.BigNumber.from("1111"))).to.be.revertedWith(
        "Your vote is being made ahead of time");
    });

    it("should revert when try to add debate with a non judge", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1660532401]);
      //await ethers.provider.send("evm_mine", []);

      await expect(this.resultOracle.connect(user_4).addJudgeDebateToSecondRound(111)).to.be.revertedWith(
        "is not a judge");
    });

    it("should set correct debate when you set with correct data", async function () {
      var vote = ethers.BigNumber.from("1010");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getSecondRoundResult());
    });

    it("should set correct debate when you set with zero", async function () {
      var vote = ethers.BigNumber.from("0");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getSecondRoundResult());
    });
  });

  describe("addJudgeDebateToThirdRound() Function", function () {
    it("should revert when try to add debate with a incorrect vote", async function () {
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(ethers.BigNumber.from("222"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it.skip("should revert when try to add debate with a incorrect vote", async function () {
      await expect(this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(ethers.BigNumber.from("111"))).to.be.revertedWith(
        "Your vote is being made ahead of time");
    });

    it("should revert when try to add debate with a non judge", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1667271601]);
      //await ethers.provider.send("evm_mine", []);

      await expect(this.resultOracle.connect(user_4).addJudgeDebateToThirdRound(111)).to.be.revertedWith(
        "is not a judge");
    });

    it("should set correct debate when you set with correct data", async function () {
      var vote = ethers.BigNumber.from("101");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getThirdRoundResult());
    });

    it("should set correct debate when you set with zero", async function () {
      var vote = ethers.BigNumber.from("0");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      expect(vote).to.eq(await this.resultOracle.connect(user).getThirdRoundResult());
    });
  });


  describe("setFirstRoundResult() Function", function () {
    it("should revert when try to set result with a incorrect vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
      //await ethers.provider.send("evm_mine", []);
      await expect(this.resultOracle.setFirstRoundResult(ethers.BigNumber.from("1101111001"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it("should revert when try to set result with not owner address", async function () {
      await expect(this.resultOracle.connect(user).setFirstRoundResult(ethers.BigNumber.from("110101"))).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when try to set result but there is not debates", async function () {
      await expect(this.resultOracle.setFirstRoundResult(ethers.BigNumber.from("110101"))).to.be.revertedWith(
        "the judges did not upload first round results");
    });

    it("should revert when try to set result but there is not enough debates", async function () {
      var vote = ethers.BigNumber.from("1100111");

      this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);
      await expect(this.resultOracle.setFirstRoundResult(vote)).to.be.revertedWith(
        "the judges did not upload first round results");
    });

    it("should return false if there is not match between 3 judges or more", async function () {
      var vote = ethers.BigNumber.from("1100111");
      var vote1 = ethers.BigNumber.from("11111");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote1);

      await this.resultOracle.setFirstRoundResult(vote);

      await expect(this.resultOracle.getFirstRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return true if there is match between 3 judges", async function () {
      var vote = ethers.BigNumber.from("1100111");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      const result = await this.resultOracle.getFirstRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return true if there is match between 3 judges and one doesnt", async function () {
      var vote = ethers.BigNumber.from("1100111");
      var vote1 = ethers.BigNumber.from("11111");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote1);

      await this.resultOracle.setFirstRoundResult(vote1);

      const result = await this.resultOracle.getFirstRoundResult();

      expect(vote1).to.eq(result);
    });

    it("should return true if there is match between 4 judges", async function () {
      var vote = ethers.BigNumber.from("1100111");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      const result = await this.resultOracle.getFirstRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return false if there is match between just 2 judges", async function () {
      var vote = ethers.BigNumber.from("1100111");
      var vote1 = ethers.BigNumber.from("1111111");
      var vote2 = ethers.BigNumber.from("1100011");

      await this.resultOracle.connect(user_3).addJudgeDebateToFirstRound(vote2);
      await this.resultOracle.connect(user_2).addJudgeDebateToFirstRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToFirstRound(vote);

      await this.resultOracle.setFirstRoundResult(vote);

      await expect(this.resultOracle.getFirstRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });
  });


  describe("setSecondRoundResult() Function", function () {
    it("should revert when try to set result with a incorrect vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
      //await ethers.provider.send("evm_mine", []);
      await expect(this.resultOracle.setSecondRoundResult(ethers.BigNumber.from("1221"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it("should revert when try to set result with not owner address", async function () {
      await expect(this.resultOracle.connect(user).setSecondRoundResult(ethers.BigNumber.from("1111"))).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when try to set result but there is not debates", async function () {
      await expect(this.resultOracle.setSecondRoundResult(ethers.BigNumber.from("1111"))).to.be.revertedWith(
        "the judges did not upload second round results");
    });

    it("should revert when try to set result but there is not enough debates", async function () {
      var vote = ethers.BigNumber.from("1001");

      this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);
      await expect(this.resultOracle.setSecondRoundResult(vote)).to.be.revertedWith(
        "the judges did not upload second round results");
    });

    it("should return false if there is not match between 3 judges or more", async function () {
      var vote = ethers.BigNumber.from("1101");
      var vote1 = ethers.BigNumber.from("110");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote1);

      await this.resultOracle.setSecondRoundResult(vote);

      await expect(this.resultOracle.getSecondRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return true if there is match between 3 judges", async function () {
      var vote = ethers.BigNumber.from("1001");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      const result = await this.resultOracle.getSecondRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return true if there is match between 3 judges and one doesnt", async function () {
      var vote = ethers.BigNumber.from("1010");
      var vote1 = ethers.BigNumber.from("1000");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote1);

      await this.resultOracle.setSecondRoundResult(vote1);

      const result = await this.resultOracle.getSecondRoundResult();

      expect(vote1).to.eq(result);
    });

    it("should return true if there is match between 4 judges", async function () {
      var vote = ethers.BigNumber.from("1101");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      const result = await this.resultOracle.getSecondRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return false if there is match between just 2 judges", async function () {
      var vote = ethers.BigNumber.from("1101");
      var vote1 = ethers.BigNumber.from("1001");
      var vote2 = ethers.BigNumber.from("11");

      await this.resultOracle.connect(user_3).addJudgeDebateToSecondRound(vote2);
      await this.resultOracle.connect(user_2).addJudgeDebateToSecondRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToSecondRound(vote);

      await this.resultOracle.setSecondRoundResult(vote);

      await expect(this.resultOracle.getSecondRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });
  });

  describe("setThirdRoundResult() Function", function () {
    it("should revert when try to set result with a incorrect vote", async function () {
      //await ethers.provider.send("evm_setNextBlockTimestamp", [1657508401]);
      //await ethers.provider.send("evm_mine", []);
      await expect(this.resultOracle.setThirdRoundResult(ethers.BigNumber.from("222"))).to.be.revertedWith(
        "its a incorrect value");
    });

    it("should revert when try to set result with not owner address", async function () {
      await expect(this.resultOracle.connect(user).setThirdRoundResult(ethers.BigNumber.from("101"))).to.be.revertedWith(
        "Ownable: caller is not the owner");
    });

    it("should revert when try to set result but there is not debates", async function () {
      await expect(this.resultOracle.setThirdRoundResult(ethers.BigNumber.from("111"))).to.be.revertedWith(
        "the judges did not upload third round results");
    });

    it("should revert when try to set result but there is not enough debates", async function () {
      var vote = ethers.BigNumber.from("100");

      this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);
      await expect(this.resultOracle.setThirdRoundResult(vote)).to.be.revertedWith(
        "the judges did not upload third round results");
    });

    it("should return false if there is not match between 3 judges or more", async function () {
      var vote = ethers.BigNumber.from("11");
      var vote1 = ethers.BigNumber.from("1");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote1);

      await this.resultOracle.setThirdRoundResult(vote);

      await expect(this.resultOracle.getThirdRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });

    it("should return true if there is match between 3 judges", async function () {
      var vote = ethers.BigNumber.from("101");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      const result = await this.resultOracle.getThirdRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return true if there is match between 3 judges and one doesnt", async function () {
      var vote = ethers.BigNumber.from("1");
      var vote1 = ethers.BigNumber.from("11");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote1);

      await this.resultOracle.setThirdRoundResult(vote1);

      const result = await this.resultOracle.getThirdRoundResult();

      expect(vote1).to.eq(result);
    });

    it("should return true if there is match between 4 judges", async function () {
      var vote = ethers.BigNumber.from("101");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote);
      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      const result = await this.resultOracle.getThirdRoundResult();

      expect(vote).to.eq(result);
    });

    it("should return false if there is match between just 2 judges", async function () {
      var vote = ethers.BigNumber.from("101");
      var vote1 = ethers.BigNumber.from("111");
      var vote2 = ethers.BigNumber.from("11");

      await this.resultOracle.connect(user_3).addJudgeDebateToThirdRound(vote2);
      await this.resultOracle.connect(user_2).addJudgeDebateToThirdRound(vote1);
      await this.resultOracle.connect(user).addJudgeDebateToThirdRound(vote);

      await this.resultOracle.setThirdRoundResult(vote);

      await expect(this.resultOracle.getThirdRoundResult()).to.be.revertedWith(
        "There is no result yet");
    });
  });

});








//setFirstRoundResult()
    //ingresar con una address no owner
    //ingresar un voto invalido
    //que exista match entre 0 y 1 y que se setee ese valor
    //que exista match entre 2 y 1 y que se setee ese valor
    //que exista match entre 0 y 2 y que se setee ese valor
    //que no exista match y setear el valor del owner.

//setSecondRoundResult()
//ingresar con una address no owner
//ingresar un voto invalido
//que exista match entre 0 y 1 y que se setee ese valor
//que exista match entre 2 y 1 y que se setee ese valor
//que exista match entre 0 y 2 y que se setee ese valor
//que no exista match y setear el valor del owner.

//setThirdRoundResult()
//ingresar con una address no owner
//ingresar un voto invalido
//que exista match entre 0 y 1 y que se setee ese valor
//que exista match entre 2 y 1 y que se setee ese valor
//que exista match entre 0 y 2 y que se setee ese valor
//que no exista match y setear el valor del owner.