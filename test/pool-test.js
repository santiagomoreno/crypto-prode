const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Pool", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const CryptoProdeCollection = await ethers.getContractFactory("CryptoProdeCollection", owner);
        this.nftContract = await CryptoProdeCollection.deploy(["Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi", "Qma9kWf7FuEUo8W4bgqzcBcNrSpszMcPuFMpbbeyLvKQQi"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address);

        await this.nftContract.setPool(this.pool.address);
        await this.stablecoin.mint(1000);
    });

    describe("buyNFT() Function", function () {
        it("Should revert when try to buy nft with 0 DAI", async function () {
            await expect(this.pool.connect(user).buyRandomNFT()).to.be.reverted;
        });

        it("Should total prize increase when try to buy nft with 50 DAI without whitelist", async function () {
            await this.stablecoin.transfer(user.address, 50);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);

            await this.pool.connect(user).buyRandomNFT();

            expect(50).to.eq(await this.pool.totalPrize());
        });

        it("Should  total prize increase when try to buy nft with 30 DAI with whitelist", async function () {
            const nftPrice = 30;
            await this.stablecoin.transfer(user.address, nftPrice);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, nftPrice);

            await this.pool.connect(user).addWhitelist();
            await this.pool.connect(user).buyRandomNFT();

            expect(nftPrice).to.eq(await this.pool.totalPrize());
        });

        it("Should  total prize increase when try to buy nft with different prices", async function () {
            const nftPriceWhitelisted = 30;
            const nftPrice = 50;

            await this.stablecoin.transfer(user.address, nftPrice);
            await this.stablecoin.transfer(user_2.address, nftPrice);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, nftPrice);
            await this.stablecoin.connect(user_2).increaseAllowance(this.pool.address, nftPrice);

            await this.pool.connect(user).addWhitelist();
            await this.pool.connect(user).buyRandomNFT();
            await this.pool.connect(user_2).buyRandomNFT();

            expect(nftPrice + nftPriceWhitelisted).to.eq(await this.pool.totalPrize());
        });

        it("Should revert when you try to buy in FIRST round UPDATING_RESULT phase", async function () {
            const nftPrice = 50;

            await this.stablecoin.transfer(user.address, nftPrice);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, nftPrice);

            await this.pool.endVotingTime();

            await expect(this.pool.connect(user).buyRandomNFT()).to.be.revertedWith(
                "you can not perform this action in this phase");
        });

        it("Should revert when you try to buy in SECOND round VOTING phase", async function () {
            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));

            await this.pool.updateFirstRoundWinners();

            await expect(this.pool.connect(user).buyRandomNFT()).to.be.revertedWith(
                "you can not perform this action in this phase");
        });

        it("Should revert when you try to buy in SECOND round UPDATING_RESULT phase", async function () {
            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.endVotingTime();

            await expect(this.pool.connect(user).buyRandomNFT()).to.be.revertedWith(
                "you can not perform this action in this phase");
        });

        it("Should revert when you try to buy in THIRD round VOTING phase", async function () {
            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.endVotingTime();

            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1010"));
            await this.pool.updateSecondRoundWinners();

            await expect(this.pool.connect(user).buyRandomNFT()).to.be.revertedWith(
                "you can not perform this action in this phase");
        });

        it("Should revert when you try to buy in THIRD round END phase", async function () {
            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.endVotingTime();

            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1010"));
            await this.pool.updateSecondRoundWinners();

            await this.pool.endVotingTime();

            await this.oracleMock.setThirdRoundResult(ethers.BigNumber.from("101"));
            await this.pool.updateThirdRoundWinners();

            await expect(this.pool.connect(user).buyRandomNFT()).to.be.revertedWith(
                "you can not perform this action in this phase");
        });
    });

    describe("vote() Function", function () {
        it("Should revert when try to vote with a nft that you are not owner", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await expect(this.pool.connect(user_2).vote(1, 101101)).to.be.revertedWith(
                "msg.sender does not own this nft");
        });

        it("Should revert when try to vote with a incorrect vote", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("11122111"))).to.be.revertedWith(
                "does not meet the characteristics of this vote");
        });

        it("Should vote in DEPOSITING phase and have the same vote in the getter", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("11011"));

            expect(ethers.BigNumber.from("11011")).to.eq(await this.pool.getFirstRoundSelection(1));
        });

        it("Should revert when try to vote for second time with the same nft", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("11011"));

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("11111"))).to.be.revertedWith(
                "you already voted");
        });

        it("Should revert when you try to vote in UPDATING_RESULT phase", async function () {
            const nftPrice = 50;
            await this.stablecoin.transfer(user.address, nftPrice);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, nftPrice);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.endVotingTime();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("11011"))).to.be.revertedWith(
                "we are not in the voting phase");
        });

        it("Should revert when you try to vote in END phase", async function () {
            const nftPrice = 50;
            await this.stablecoin.transfer(user.address, nftPrice);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, nftPrice);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.endVotingTime();

            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1010"));
            await this.pool.updateSecondRoundWinners();

            await this.pool.endVotingTime();

            await this.oracleMock.setThirdRoundResult(ethers.BigNumber.from("101"));
            await this.pool.updateThirdRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("100"))).to.be.revertedWith(
                "we are not in the voting phase");
        });

        it("Should vote in SECOND round and VOTING phase and have the same vote in the getter", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("11111"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"));

            expect(ethers.BigNumber.from("1111")).to.eq(await this.pool.getSecondRoundSelection(1));
        });

        it("Should revert when you were removed and try to vote anyway", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("00000"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11111"));
            await this.pool.updateFirstRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"))).to.be.revertedWith(
                "you did not pass the first round");
        });

        it("Should revert when you were removed and try to vote in SECOND round anyway with 4 asserts", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("11111111"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"))).to.be.revertedWith(
                "you did not pass the first round");
        });

        it("Should vote in SECOND round and VOTING phase and have the same vote in the getter with 5 asserts", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10111111"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"));

            expect(ethers.BigNumber.from("1111")).to.eq(await this.pool.getSecondRoundSelection(1));
        });

        it("Should revert when vote in SECOND round with a incorrect vote", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("111111"))).to.be.revertedWith(
                "does not meet the characteristics of this vote");
        });

        it("Should revert whe you try to vote for second time in SECOND round", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10111111"));

            await this.pool.endVotingTime();

            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"));

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("1010"))).to.be.revertedWith(
                "you already voted");
        });

        it("Should vote in THIRD round when pass SECOND round", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1011"));
            await this.pool.updateSecondRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("101"));

            expect(ethers.BigNumber.from("101")).to.eq(await this.pool.getThirdRoundSelection(1));
        });

        it("Should revert when try to vote THIRD round and loss in second", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("0"));
            await this.pool.endVotingTime();
            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("11"));
            await this.pool.updateSecondRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("101"))).to.be.revertedWith(
                "you did not pass the second round");
        });

        it("Should revert when try to vote THIRD round and loss in FIRST", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1"));
            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.endVotingTime();
            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("11"));
            await this.pool.updateSecondRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("101"))).to.be.revertedWith(
                "you did not pass the second round");
        });

        it("Should revert when try to vote THIRD round with a incorrect vote", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1010"));
            await this.pool.updateSecondRoundWinners();

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("1111"))).to.be.revertedWith(
                "does not meet the characteristics of this vote");
        });

        it("Should revert when try to vote THIRD round for second time ", async function () {
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.pool.connect(user).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("10101010"));
            await this.pool.updateFirstRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("1010"));
            await this.pool.endVotingTime();
            await this.oracleMock.setSecondRoundResult(ethers.BigNumber.from("1010"));
            await this.pool.updateSecondRoundWinners();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("111"));

            await expect(this.pool.connect(user).vote(1, ethers.BigNumber.from("101"))).to.be.revertedWith(
                "you already voted");
        });
    });

    describe("amountToClaim() Function", function () {
        it("Should give all 20% first round to one winner", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.transfer(user_2.address, 50);
            await this.stablecoin.transfer(user_3.address, 50);
            await this.stablecoin.transfer(user_4.address, 50);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_2).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_3).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_4).increaseAllowance(this.pool.address, 50);

            await this.pool.connect(user).buyRandomNFT();
            await this.pool.connect(user_2).buyRandomNFT();
            await this.pool.connect(user_3).buyRandomNFT();
            await this.pool.connect(user_4).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010")); //4 asserts
            await this.pool.connect(user_2).vote(2, ethers.BigNumber.from("11111111"));//4 asserts
            await this.pool.connect(user_3).vote(3, ethers.BigNumber.from("10101011"));//3 asserts
            await this.pool.connect(user_4).vote(4, ethers.BigNumber.from("11110000"));//8 asserts

            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11110000"));
            await this.pool.updateFirstRoundWinners();

            expect(ethers.BigNumber.from("0")).to.eq(await this.pool.connect(user).amountToClaim(1));
            expect(ethers.BigNumber.from("0")).to.eq(await this.pool.connect(user_2).amountToClaim(2));
            expect(ethers.BigNumber.from("0")).to.eq(await this.pool.connect(user_3).amountToClaim(3));
            expect(ethers.BigNumber.from("40")).to.eq(await this.pool.connect(user_4).amountToClaim(4));
            //total pool = 200 DAIs, 20% first round = 40 DAIS 
        });

        it("Should give all 20% first round to two winners", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.transfer(user_2.address, 50);
            await this.stablecoin.transfer(user_3.address, 50);
            await this.stablecoin.transfer(user_4.address, 50);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_2).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_3).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_4).increaseAllowance(this.pool.address, 50);

            await this.pool.connect(user).buyRandomNFT();
            await this.pool.connect(user_2).buyRandomNFT();
            await this.pool.connect(user_3).buyRandomNFT();
            await this.pool.connect(user_4).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("10101010")); //4 asserts
            await this.pool.connect(user_2).vote(2, ethers.BigNumber.from("11111111"));//4 asserts
            await this.pool.connect(user_3).vote(3, ethers.BigNumber.from("11111100"));//6 asserts
            await this.pool.connect(user_4).vote(4, ethers.BigNumber.from("11110000"));//8 asserts

            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11110000"));
            await this.pool.updateFirstRoundWinners();

            expect(ethers.BigNumber.from("0")).to.eq(await this.pool.connect(user).amountToClaim(1));
            expect(ethers.BigNumber.from("0")).to.eq(await this.pool.connect(user_2).amountToClaim(2));
            expect(ethers.BigNumber.from("20")).to.eq(await this.pool.connect(user_3).amountToClaim(3));
            expect(ethers.BigNumber.from("20")).to.eq(await this.pool.connect(user_4).amountToClaim(4));
            //total pool = 200 DAIs, 20% first round = 40 DAIS 
        });

        it("Should give all 20% first round to four winners", async function () {
            //buy NFT
            await this.stablecoin.transfer(user.address, 50);
            await this.stablecoin.transfer(user_2.address, 50);
            await this.stablecoin.transfer(user_3.address, 50);
            await this.stablecoin.transfer(user_4.address, 50);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_2).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_3).increaseAllowance(this.pool.address, 50);
            await this.stablecoin.connect(user_4).increaseAllowance(this.pool.address, 50);

            await this.pool.connect(user).buyRandomNFT();
            await this.pool.connect(user_2).buyRandomNFT();
            await this.pool.connect(user_3).buyRandomNFT();
            await this.pool.connect(user_4).buyRandomNFT();

            await this.pool.connect(user).vote(1, ethers.BigNumber.from("11111000")); //5 asserts
            await this.pool.connect(user_2).vote(2, ethers.BigNumber.from("11111100"));//6 asserts
            await this.pool.connect(user_3).vote(3, ethers.BigNumber.from("11111100"));//6 asserts
            await this.pool.connect(user_4).vote(4, ethers.BigNumber.from("11110000"));//8 asserts

            await this.pool.endVotingTime();
            await this.oracleMock.setFirstRoundResult(ethers.BigNumber.from("11110000"));
            await this.pool.updateFirstRoundWinners();

            expect(ethers.BigNumber.from("10")).to.eq(await this.pool.connect(user).amountToClaim(1));
            expect(ethers.BigNumber.from("10")).to.eq(await this.pool.connect(user_2).amountToClaim(2));
            expect(ethers.BigNumber.from("10")).to.eq(await this.pool.connect(user_3).amountToClaim(3));
            expect(ethers.BigNumber.from("10")).to.eq(await this.pool.connect(user_4).amountToClaim(4));
            //total pool = 200 DAIs, 20% first round = 40 DAIS 
        });

    });

});




//cosas a testear
/**
 * amountToClaim
 * claim
 * redistributeRewardsNoWinnerInThirdRound
 * devTeamReceiveFunds
 * check complex cases with results with differents lengths
 */

//cosas testeadas
/**¨
 * getFirstRoundSelection
 * getSecondRoundSelection
 * getThirdRoundSelection
 * getNFTPrice
 * buyRandomNFT
 * vote
 * endVotingTime
 * updateFirstRoundWinners
 * isWinnerOfFirstRound
 * updateSecondRoundWinners
 * isWinnerOfSecondRound
 * updateThirdRoundWinners
 * isWinnerOfThirdRound
 * addWhitelist
 * onlyPhase
 * onlyRound
 */


