pragma solidity 0.8.13;

contract OracleMock {
    uint32 private _firstRoundResult = type(uint32).max;
    uint16 private _secondRoundResult = type(uint16).max;
    uint8 private _thirdRoundResult = type(uint8).max;

    function getFirstRoundResult() external view returns (uint32) {
        return _firstRoundResult;
    }

    function getSecondRoundResult() external view returns (uint16) {
        return _secondRoundResult;
    }

    function getThirdRoundResult() external view returns (uint8) {
        return _thirdRoundResult;
    }

    function setFirstRoundResult(uint32 firstRoundResult) external {
        _firstRoundResult = firstRoundResult;
    }

    function setSecondRoundResult(uint16 secondRoundResult) external {
        _secondRoundResult = secondRoundResult;
    }

    function setThirdRoundResult(uint8 thirdRoundResult) external {
        _thirdRoundResult = thirdRoundResult;
    }
}
