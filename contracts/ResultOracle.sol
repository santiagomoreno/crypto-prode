pragma solidity 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";
import "hardhat/console.sol";

contract ResultOracle is Ownable {
    address[] private _judges = new address[](3);
    uint256 private constant NUMBER_OF_JUDGES = 3;

    bool[] private _firstRoundResult = new bool[](8);
    bool[] private _secondRoundResult = new bool[](4);
    bool[] private _thirdRoundResult = new bool[](3);

    mapping(address => bool[]) public _firstRoundDebate;
    mapping(address => bool[]) public _secondRoundDebate;
    mapping(address => bool[]) public _thirdRoundDebate;

    constructor(address[] memory judges) {
        require(judges.length == NUMBER_OF_JUDGES, "you must set 3 judges");
        for (uint8 i; i < _judges.length; i++) {
            require(judges[i] != address(0), "a judge cant be address zero");
            require(judges[i] != msg.sender, "the owner cant be judge");
            require(
                !isJudge(judges[i]),
                "you can't set the same address twice as a judge"
            );
            _judges[i] = judges[i];
        }
    }

    function getFirstRoundResult() external view returns (bool[] memory) {
        require(youAlreadyVoted(_firstRoundResult), "There is no result yet");
        return _firstRoundResult;
    }

    function getSecondRoundResult() external view returns (bool[] memory) {
        require(youAlreadyVoted(_secondRoundResult), "There is no result yet");
        return _secondRoundResult;
    }

    function getThirdRoundResult() external view returns (bool[] memory) {
        require(youAlreadyVoted(_thirdRoundResult), "There is no result yet");
        return _thirdRoundResult;
    }

    function youAlreadyVoted(bool[] memory _vote) internal pure returns (bool) {
        for (uint8 i; i < _vote.length; ) {
            if (_vote[i]) return true;
            unchecked {
                ++i;
            }
        }
        return false;
    }

    function getJudge(uint8 index) external view returns (address) {
        return _judges[index];
    }

    function getAmountOfJudge() public view returns (uint256) {
        return _judges.length;
    }

    function setNewJudge(uint8 index, address newJudge) external onlyOwner {
        //require(block.timestamp < 1653879599, "expired to set a judge"); //29/05 23:59:59
        require(index < NUMBER_OF_JUDGES, "incorrect index");
        require(newJudge != address(0), "a judge cant be address zero");
        require(newJudge != msg.sender, "the owner cant be judge");
        require(
            !isJudge(newJudge),
            "you can't set the same address twice as a judge"
        );
        _judges[index] = newJudge;
    }

    //add judge debate
    function addJudgeDebateToFirstRound(bool[] memory vote) external {
        require(vote.length == 8, "its a incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        /*require(
            block.timestamp > 1657508400, //11/07 00:01
            "Your vote is being made ahead of time"
        );*/
        _firstRoundDebate[msg.sender] = vote;
    }

    function addJudgeDebateToSecondRound(bool[] memory vote) external {
        require(vote.length == 4, "its a incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        /*require(
            block.timestamp > 1660532400, // 15/08 00:01
            "Your vote is being made ahead of time"
        );*/
        _secondRoundDebate[msg.sender] = vote;
    }

    function addJudgeDebateToThirdRound(bool[] memory vote) external {
        require(vote.length == 3, "its a incorrect value");
        require(isJudge(msg.sender), "you are not a judge");
        //require(
        //    block.timestamp > 1667271600, // 01/11 00:01
        //    "Your vote is being made ahead of time"
        //);
        _thirdRoundDebate[msg.sender] = vote;
    }

    //set final vote
    function setFirstRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 8, "its a incorrect value");
        require(
            isJudgesAddedFirstRoundResults(),
            "the judges did not upload first round results"
        );
        //its not necesary validate the blocktimestamp, because the validation is in judge debate vote, and this just works with these params setted
        for (uint256 i; i < vote.length; ) {
            uint8 flat;
            address judge1 = _judges[0];
            address judge2 = _judges[1];
            address judge3 = _judges[2];
            if (_firstRoundDebate[judge1][i]) ++flat;
            if (_firstRoundDebate[judge2][i]) ++flat;
            if (_firstRoundDebate[judge3][i]) ++flat;
            if (vote[i]) ++flat;

            if (flat > 2) {
                _firstRoundResult[i] = true;
            } else {
                _firstRoundResult[i] = false;
            }
            unchecked {
                ++i;
            }
        }
    }

    function isJudgesAddedFirstRoundResults() private view returns (bool) {
        uint8 resultsAmounts;
        for (uint8 i; i < _judges.length; ) {
            if (youAlreadyVoted(_firstRoundDebate[_judges[i]])) {
                resultsAmounts++;
            }
            unchecked {
                ++i;
            }
        }
        if (resultsAmounts >= 2) return true;
        return false;
    }

    function setSecondRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 4, "its a incorrect value");
        require(
            isJudgesAddedSecondRoundResults(),
            "the judges did not upload second round results"
        );
        //its not necesary validate the blocktimestamp, because the validation is in judge debate vote, and this just works with these params setted
        for (uint256 i; i < vote.length; ) {
            uint8 flat;
            address judge1 = _judges[0];
            address judge2 = _judges[1];
            address judge3 = _judges[2];
            if (_secondRoundDebate[judge1][i]) ++flat;
            if (_secondRoundDebate[judge2][i]) ++flat;
            if (_secondRoundDebate[judge3][i]) ++flat;
            if (vote[i]) ++flat;

            if (flat > 2) {
                _secondRoundResult[i] = true;
            } else {
                _secondRoundResult[i] = false;
            }
            unchecked {
                ++i;
            }
        }
    }

    function isJudgesAddedSecondRoundResults() private view returns (bool) {
        uint8 resultsAmounts;
        for (uint8 i; i < _judges.length; ) {
            if (youAlreadyVoted(_secondRoundDebate[_judges[i]])) {
                resultsAmounts++;
            }
            unchecked {
                ++i;
            }
        }
        if (resultsAmounts >= 2) return true;
        return false;
    }

    function setThirdRoundResult(bool[] memory vote) external onlyOwner {
        require(vote.length == 3, "its a incorrect value");
        require(
            isJudgesAddedThirdRoundResults(),
            "the judges did not upload third round results"
        );
        //its not necesary validate the blocktimestamp, because the validation is in judge debate vote, and this just works with these params setted
        for (uint256 i; i < vote.length; ) {
            uint8 flat;
            address judge1 = _judges[0];
            address judge2 = _judges[1];
            address judge3 = _judges[2];
            if (_thirdRoundDebate[judge1][i]) ++flat;
            if (_thirdRoundDebate[judge2][i]) ++flat;
            if (_thirdRoundDebate[judge3][i]) ++flat;
            if (vote[i]) ++flat;

            if (flat > 2) {
                _thirdRoundResult[i] = true;
            } else {
                _thirdRoundResult[i] = false;
            }
            unchecked {
                ++i;
            }
        }
    }

    function isJudgesAddedThirdRoundResults() private view returns (bool) {
        uint8 resultsAmounts;
        for (uint8 i; i < _judges.length; ) {
            if (youAlreadyVoted(_thirdRoundDebate[_judges[i]])) {
                resultsAmounts++;
            }
            unchecked {
                ++i;
            }
        }
        if (resultsAmounts >= 2) return true;
        return false;
    }

    //boolean is judge
    function getIndexJudge(address aspirant) public view returns (uint8) {
        for (uint8 i = 0; i < _judges.length; ) {
            if (_judges[i] == aspirant) {
                return i;
            }
            unchecked {
                i++;
            }
        }
        revert("is not a judge");
    }

    function isJudge(address aspirant) private view returns (bool) {
        if (aspirant == _judges[0]) return true;
        if (aspirant == _judges[1]) return true;
        if (aspirant == _judges[2]) return true;
        return false;
    }
}
