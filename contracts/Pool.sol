pragma solidity 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

interface IResultOracle {
    function getFirstRoundResult() external returns (bool[] memory);

    function getSecondRoundResult() external returns (bool[] memory);

    function getThirdRoundResult() external returns (bool[] memory);
}

interface INFTCollection {
    function _balanceOf(address account, uint256 id)
        external
        view
        returns (uint256);

    function getOwnerNftId(uint256 nftId) external view returns (address);

    function mint(address owner, uint8 idTeam) external;

    function randomMint(address owner) external returns (uint16);

    function totalBalanceOf(address account)
        external
        view
        returns (uint256 amount);

    function balanceOfEachToken(address account)
        external
        view
        returns (uint256[] memory amount);
}

contract Pool is Ownable {
    enum Round {
        FIRST,
        SECOND,
        THIRD
    }
    enum Phase {
        DEPOSITING,
        VOTING,
        UPDATING_RESULT,
        END
    }

    using Strings for uint256;

    mapping(uint16 => bool[]) private _firstRoundSelection; //0 to 4294967295
    mapping(uint16 => bool[]) private _secondRoundSelection; // 0 to 65535
    mapping(uint16 => bool[]) private _thirdRoundSelection; // 0 to 255

    mapping(uint16 => uint256) private _withdrawalAmount; //cantidad que se retiro de cada nft

    mapping(uint16 => bool) private _firstRoundWinners;
    mapping(uint16 => bool) private _secondRoundWinners;
    mapping(uint16 => bool) private _thirdRoundWinners;
    uint16 public _amountFirstRoundWinners; //son 1600 y uint16 = 65535
    uint16 public _amountSecondRoundWinners; //son 1600 y uint16 = 65535
    uint16 public _amountThirdRoundWinners; //son 1600 y uint16 = 65535
    uint16 private _givenWhitelist;
    uint16 private _participants; //son 1600 y uint16 = 65535
    uint8 public constant NFT_PRICE = 50;
    uint8 public constant NFT_WHITELISTED_PRICE = 30;
    uint256 public totalPrize;
    bool public isEvenAWinner;

    Round public _actualRound = Round.FIRST;
    Phase public _actualPhase = Phase.DEPOSITING;

    IResultOracle private _resultOracle;
    IERC20 private _stablecoinToken;
    INFTCollection private _nftContract;

    mapping(address => bool) private _whitelist;

    constructor(
        IERC20 stablecoinToken,
        INFTCollection nftContract,
        IResultOracle resultOracle
    ) {
        _stablecoinToken = stablecoinToken;
        _nftContract = nftContract;
        _resultOracle = resultOracle;
    }

    //getters
    function getFirstRoundSelection(uint16 nftId)
        external
        view
        returns (bool[] memory)
    {
        return _firstRoundSelection[nftId];
    }

    function getSecondRoundSelection(uint16 nftId)
        external
        view
        returns (bool[] memory)
    {
        return _secondRoundSelection[nftId];
    }

    function getThirdRoundSelection(uint16 nftId)
        external
        view
        returns (bool[] memory)
    {
        return _thirdRoundSelection[nftId];
    }

    function amountToClaim(uint16 nftId) public view returns (uint256 amount) {
        uint256 totalRewards;
        if (_firstRoundWinners[nftId]) {
            totalRewards = ((totalPrize * 20) / 100) / _amountFirstRoundWinners;
            //20% del prize dividido la cant de ganadores.
        }

        if (_secondRoundWinners[nftId]) {
            totalRewards +=
                ((totalPrize * 25) / 100) /
                _amountSecondRoundWinners;
            //25% del prize dividido la cant de ganadores.
        }

        if (_thirdRoundWinners[nftId]) {
            totalRewards +=
                ((totalPrize * 35) / 100) /
                _amountThirdRoundWinners;
            //35% del prize dividido la cant de ganadores.
        }

        if (redistributeRewardsNoWinnerInThirdRound()) {
            totalRewards +=
                ((totalPrize * 100) / 35) /
                _amountSecondRoundWinners;
            //35% del prize dividido la cant de ganadores de segunda ronda, ya que nadie gano.
        }

        unchecked {
            amount = totalRewards - _withdrawalAmount[nftId];
        }
    }

    function getNFTPrice(address minter) public view returns (uint256) {
        if (_whitelist[minter] && _nftContract.totalBalanceOf(minter) == 0) {
            return NFT_WHITELISTED_PRICE;
        }
        return NFT_PRICE;
    }

    //mutable methods

    //mint NFTS
    function buyRandomNFT() external {
        onlyPhase(Phase.DEPOSITING);
        uint256 price = getNFTPrice(msg.sender);

        //need to have allowance
        _stablecoinToken.transferFrom(msg.sender, address(this), price);
        uint16 nftId = _nftContract.randomMint(msg.sender);
        totalPrize = totalPrize + price;
        _participants++;
        _firstRoundSelection[nftId] = new bool[](8);
        _secondRoundSelection[nftId] = new bool[](4);
        _thirdRoundSelection[nftId] = new bool[](3);

        emit minted(msg.sender, nftId);
    }

    //voting
    function vote(uint16 nftId, bool[] memory _vote) external {
        require(
            msg.sender == _nftContract.getOwnerNftId(nftId),
            "msg.sender does not own this nft"
        );
        require(
            _actualPhase == Phase.VOTING || _actualPhase == Phase.DEPOSITING,
            "we are not in the voting phase"
        );

        if (_actualRound == Round.FIRST) {
            require(
                _vote.length == 8,
                "does not meet the characteristics of this vote"
            );
            require(
                !youAlreadyVoted(_firstRoundSelection[nftId]),
                "you already voted"
            );
            _firstRoundSelection[nftId] = _vote;
        } else if (_actualRound == Round.SECOND) {
            require(
                _firstRoundWinners[nftId],
                "you did not pass the first round"
            );
            require(
                _vote.length == 4,
                "does not meet the characteristics of this vote"
            );
            require(
                !youAlreadyVoted(_secondRoundSelection[nftId]),
                "you already voted"
            );

            _secondRoundSelection[nftId] = _vote;
        } else if (_actualRound == Round.THIRD) {
            require(
                _secondRoundWinners[nftId],
                "you did not pass the second round"
            );
            require(
                _vote.length == 3,
                "does not meet the characteristics of this vote"
            );
            require(
                !youAlreadyVoted(_thirdRoundSelection[nftId]),
                "you already voted"
            );
            _thirdRoundSelection[nftId] = _vote;
        } else {
            revert("round is wrong");
        }

        emit voted(msg.sender, _actualRound);
    }

    function youAlreadyVoted(bool[] memory _vote) internal view returns (bool) {
        for (uint8 i; i < _vote.length; ) {
            if (_vote[i]) return true;
            unchecked {
                ++i;
            }
        }
        return false;
    }

    //claim
    function claim(uint16 nftId) external returns (uint256) {
        require(
            msg.sender == _nftContract.getOwnerNftId(nftId),
            "msg.sender does not own this nft"
        );
        if (_actualRound == Round.FIRST) return 0;

        uint256 amount = amountToClaim(nftId);

        _withdrawalAmount[nftId] += amount;

        require(amount != 0, "nothing to claim");

        _stablecoinToken.transfer(msg.sender, amount);

        return amount;
    }

    //admin methods - change phase methods
    function endVotingTime() external onlyOwner {
        if (_actualPhase == Phase.VOTING || _actualPhase == Phase.DEPOSITING) {
            _actualPhase = Phase.UPDATING_RESULT;
        }
    }

    //update first round winners - owner method
    function updateFirstRoundWinners() external onlyOwner {
        onlyPhase(Phase.UPDATING_RESULT);
        onlyRound(Round.FIRST);
        uint16 amountWinners;
        bool[] memory result = _resultOracle.getFirstRoundResult();
        for (uint16 i; i <= _participants; ) {
            if (isWinnerOfFirstRound(i, uint256(result))) {
                _firstRoundWinners[i] = true;
                amountWinners += 1;
            }
            unchecked {
                i++;
            }
        }
        _amountFirstRoundWinners = amountWinners;
        _actualPhase = Phase.VOTING;
        _actualRound = Round.SECOND;
    }

    function isWinnerOfFirstRound(uint16 nftId, uint256 result)
        private
        view
        returns (bool)
    {
        uint256 selection = uint256(_firstRoundSelection[nftId]);
        uint8 success;
        for (uint256 i; i < bytes(selection.toString()).length; ) {
            if (bytes(selection.toString())[i] == bytes(result.toString())[i]) {
                success++;
            }
            unchecked {
                i++;
            }
        }
        if (success > 4) return true;
        return false;
    }

    //update second round winners - owner method
    function updateSecondRoundWinners() external onlyOwner {
        onlyPhase(Phase.UPDATING_RESULT);
        onlyRound(Round.SECOND);
        uint16 amountWinners;
        uint16 result = _resultOracle.getSecondRoundResult();
        for (uint16 i; i <= _participants; ) {
            if (isWinnerOfSecondRound(i, uint256(result))) {
                _secondRoundWinners[i] = true;
                amountWinners += 1;
            }
            unchecked {
                i++;
            }
        }
        _amountSecondRoundWinners = amountWinners;
        _actualPhase = Phase.VOTING;
        _actualRound = Round.THIRD;
    }

    function isWinnerOfSecondRound(uint16 nftId, uint256 result)
        private
        view
        returns (bool)
    {
        uint256 selection = uint256(_secondRoundSelection[nftId]);
        uint8 success;
        for (uint256 i; i < bytes(selection.toString()).length; ) {
            if (bytes(selection.toString())[i] == bytes(result.toString())[i]) {
                success++;
            }
            unchecked {
                i++;
            }
        }
        if (success > 2) return true;
        return false;
    }

    //update third round winners - owner method
    function updateThirdRoundWinners() external onlyOwner {
        onlyPhase(Phase.UPDATING_RESULT);
        onlyRound(Round.THIRD);
        uint16 amountWinners;
        uint8 result = _resultOracle.getThirdRoundResult();
        for (uint16 i; i <= _participants; ) {
            if (isWinnerOfThirdRound(i, uint256(result))) {
                _thirdRoundWinners[i] = true;
                amountWinners += 1;
                isEvenAWinner = true;
            }
            unchecked {
                i++;
            }
        }
        _amountThirdRoundWinners = amountWinners;
        _actualPhase = Phase.END;
    }

    function isWinnerOfThirdRound(uint16 nftId, uint256 result)
        private
        view
        returns (bool)
    {
        uint256 selection = uint256(_thirdRoundSelection[nftId]);
        uint8 success;
        for (uint256 i; i < bytes(selection.toString()).length; ) {
            if (bytes(selection.toString())[i] == bytes(result.toString())[i]) {
                //00000111  //10001111
                success++;
            }
            unchecked {
                i++;
            }
        }
        if (success == 3) return true;
        return false;
    }

    function redistributeRewardsNoWinnerInThirdRound()
        internal
        view
        returns (bool)
    {
        //fase final, no tiene que haber un ganador todavia, tiene que ser despues del 5/10 23:59
        if (
            _actualPhase == Phase.END &&
            isEvenAWinner == false &&
            block.timestamp > 1665025199
        ) {
            return true;
        } else {
            return false;
        }
    }

    function devTeamReceiveFunds() external onlyOwner {
        onlyPhase(Phase.END);
        onlyRound(Round.THIRD);
        uint256 amount = (totalPrize * 10) / 100; //10% of pool
        _stablecoinToken.transfer(owner(), amount);
    }

    //whitelist
    function addWhitelist() external {
        //require(block.timestamp < 1654484399); //5/06 23:59hs
        require(_givenWhitelist < 320, "There is no more whitelist to give");
        _givenWhitelist++;
        _whitelist[msg.sender] = true;
    }

    function onlyPhase(Phase expected) private view {
        require(
            _actualPhase == expected,
            "you can not perform this action in this phase"
        );
    }

    function onlyRound(Round expected) private view {
        require(_actualRound == expected);
    }

    event claimed(address claimer, uint256 amount);
    event voted(address voter, Round round);
    event minted(address minter, uint16 nftId);
}
