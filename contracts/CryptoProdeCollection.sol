pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract CryptoProdeCollection is ERC1155Supply, Ownable {
    uint256 private tokenId;
    string[] private _hashIpfs;

    address private _poolContract;
    mapping(uint256 => address) private nftOwners;
    mapping(address => uint256[]) private addessNft;

    constructor(string[] memory hashes) ERC1155("https://ipfs.io/ipfs/{id}") {
        _hashIpfs = hashes;
    }

    function uri(uint256 id) public view override returns (string memory) {
        return string(abi.encodePacked("https://ipfs.io/ipfs/", _hashIpfs[id]));
    }

    function randomMint(address minter) external returns (bool) {
        onlyPool();
        for (uint16 i; i < type(uint16).max; i++) {
            uint256 randomNftId = randomNumber();
            if (totalSupply(randomNftId) < 101) {
                _mint(minter, randomNftId, 1, "");
                tokenId++;
                nftOwners[tokenId] = minter;
                addessNft[minter].push(tokenId);
                break;
            }
        }
        return true;
    }

    function getOwnerNftId(uint256 nftId) external view returns (address) {
        return nftOwners[nftId];
    }

    function getNftFrom(address owner)
        external
        view
        returns (uint256[] memory)
    {
        return addessNft[owner];
    }

    function onlyPool() private view {
        require(msg.sender == _poolContract, "only Pool");
    }

    function setPool(address pool) external onlyOwner {
        _poolContract = pool;
    }

    function totalBalanceOf(address account)
        external
        view
        returns (uint256 amount)
    {
        for (uint256 i; i < _hashIpfs.length; i++) {
            amount += balanceOf(account, i);
        }
    }

    function _balanceOf(address account, uint256 id)
        external
        view
        returns (uint256)
    {
        return balanceOf(account, id);
    }

    function balanceOfEachToken(address account)
        external
        view
        returns (uint256[] memory amount)
    {
        for (uint256 i; i < _hashIpfs.length; i++) {
            amount[i] = balanceOf(account, i);
        }
    }

    function randomNumber() private view returns (uint256 answer) {
        answer =
            uint256(
                keccak256(
                    abi.encodePacked(block.timestamp, "cryptoprode", tokenId)
                )
            ) %
            16;
    }
}
