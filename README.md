
# Crypto Prode

  

El objetivo de este proyecto es realizar un pronostico deportivo (PRODE) sobre la Copa Libertadores.

Mintearemos `3200 NFT's` , de los cuales existirán 16 tipos distintos (200 de cada uno) que representaran los 16 equipos que se encuentran jugando los octavos de final de la Copa Libertadores.

  

El minteo se realizara de manera aleatoria, aunque pagando una pequeña diferencia extra se podrá elegir el equipo que se desee (si esta disponible). La moneda de pago y los beneficios van a ser realizados en `DAI's`.

  

Al terminar el periodo de minteo, todo los tokens que se recolectaran irán a un pool. En ese pool, los owners de `NFT's` podrán seleccionar que equipos pasan a la próxima ronda (no se selecciona una cantidad de goles, solo que equipo pasa de ronda). La selección solo se realiza de la ronda actual, no de todo el torneo.

  

Si se cumple que:

  

**Octavos:** 5 o mas resultados correctos de 8 partidos -> `pasa de ronda.`

  

**Cuartos:** 3 o mas resultados correctos de 4 partidos -> `pasa de ronda.`

  

**Semis y Final:** 3 partidos con resultado correcto de 3 partidos -> `gana el torneo`

  

Entre ronda y ronda, si un owner tuvo la cantidad de predicciones necesarias, podrá cobrar en ese lapso de tiempo los tokens repartidos en esa ronda, sino las cobra, en el próximo lapso podrá cobrarlos (cuando termine el torneo, quedara abierto para cobrar en el momento que se desee) .

  

Los periodos van a ser los siguientes :

1)   `Compra/minteo de NFT's `.

2) `Selección de decisión de octavos.`

3) `La espera de los resultados (periodo de cobro).`

4)  `Selección de decisión de cuartos.`

5) `La espera de los resultados (periodo de cobro).`

6)   `Selección de decisión de semis/final.`

7)  `La espera de los resultados (periodo de cobro).`

  

Vale aclarar que cada periodo es restrictivo de todos los demás, por lo tanto, solo se va poder mintear en el primer periodo, solo se va poder decidir en los momentos de selección y **solo se va poder retirar las ganancias en el periodo de ganancias** .

  

# Distribución del Pool

  

80% - Predicción deportiva (en cada ronda se repartirá un porcentaje del total).

- Octavos a Cuartos será un 20% entre todos los usuarios que pasen.

- Cuartos a Semi será un 25% entre todos los usuarios que pasen.

- Semis y Final será un 35% entre todos los usuarios que pasen.

  

10% - Equipo ganador del torneo (Si gana Boca, todos los owners del NFT de Boca recibirán el proporcional de ese 10%).

  

10% - Equipo de devs (costos de deploy, pagina web, sueldos, etc.) Este porcentaje lo recibirá el equipo al finalizar el torneo.

  
  

## Smart contracts

  

Aquí se explicara de manera detallada como funciona cada contrato y que responsabilidad tiene.

  

## ProdePool.sol

  

Este contrato será el contrato main, responsable de:

- Resguardar todas las criptomonedas del proyecto,

- Todos los resultados que los usuarios ingresen en cada ronda,

- El manejo de etapas (phases) y permisos de que métodos se pueden ejecutar y cuales no,

- Retirar ganancias,

- Manejar los datos ingresados por el oráculo, (si pasan 7 dias sin que se suban los resultados, todo el dinero estará disponible para que los usuarios reclamen su inversión).

  

## ResultOracle.sol

  

Al no poseer un oráculo descentralizado, ya que esta funcionalidad no esta implementada en ninguna plataforma como por ejemplo **Chainlink** decidimos contar con algunos mecanismos para reducir la confianza sobre el equipo de desarrolladores.

  

Cuando se crea este contrato el owner define 4 wallets mas con las que formara un "comité de la verdad", este comité estará conformado por personas que pertenecen al proyecto.

  

Estas 5 wallets, tendrán la potestad de subir al oráculo la información con los resultados y para que un resultado tenga efecto sobre el sistema, se deberá formar un consenso en donde 4 de las 5 wallets suban exactamente los mismos resultados.

  

Este mecanismo, brinda seguridad en cuanto a que:

- Si hay una sola wallet, el que lo suba puede subir un dato erróneo (buscando algún beneficio o no).

- Que esa persona sea hackeada y sea utilizada por otro usuario que busca un beneficio.

- Que esta persona, pierda las claves privadas.

  

Todavía seguirá existiendo un nivel de confianza en cuanto a que se debe creer en que las intenciones del equipo que desarrolla son buenas, ya que las 5 wallets que acuerden los resultados formaran partes de distintas personas dentro del proyecto (ningún usuario dentro del comité, conocerá la clave privada de otro miembro).

  
  
  
  
  
  

## Tal vez en algun momento sirva. ->

## SmartyPants

  

SmartyPants converts ASCII punctuation characters into "smart" typographic punctuation HTML entities. For example:

  

| |ASCII |HTML |

|----------------|-------------------------------|-----------------------------|

|Single backticks|`'Isn't this fun?'` |'Isn't this fun?' |

|Quotes |`"Isn't this fun?"` |"Isn't this fun?" |

|Dashes |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|